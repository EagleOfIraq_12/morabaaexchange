package team.morabaa.com.morabaaexchange;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import team.morabaa.com.morabaaexchange.utils.SaveSharedPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {
    Button btnSignin;
    Button btnSignup;
    EditText txtUser;
    EditText txtPassword;
    ProgressBar prgBar;
    String userName;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(PublicClass.fontCiro)
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (SaveSharedPreference.getUserName(this).length() > 2) {
            userName = SaveSharedPreference.getUserName(this);
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.putExtra("lecName", userName);
            Toast.makeText(LoginActivity.this, userName, Toast.LENGTH_LONG).show();
            startActivity(intent);
        }
        txtUser = (EditText) findViewById(R.id.email);
        txtPassword = (EditText) findViewById(R.id.password);
    }

    @Override
    protected void onStart() {
        super.onStart();
        btnSignin = (Button) findViewById(R.id.email_sign_in_button);
        btnSignup = (Button) findViewById(R.id.email_sign_up_button);
        prgBar = (ProgressBar) findViewById(R.id.login_progress);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSignin.setVisibility(View.GONE);
                btnSignup.setVisibility(View.GONE);
                prgBar.setVisibility(View.VISIBLE);
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        boolean f;
        btnSignin = (Button) findViewById(R.id.email_sign_in_button);
        userName = txtUser.getText().toString();
        String password = txtPassword.getText().toString();
        if ((
                userName.equals("")
                        &&
                        password.equals(""))
                ||
                1 == 1) {
            f = true;
        }
        if (!f)
            Toast.makeText(LoginActivity.this, "Wrong password or Email", Toast.LENGTH_LONG).show();
        else {
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.putExtra("userName", userName);
            SaveSharedPreference.setUserName(LoginActivity.this, userName);
            startActivity(intent);
            txtPassword.setText("");
            txtUser.setText("");
        }
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    public void goToSignUpActivity(View view) {
        Intent signUpIntent = new Intent(this, SignUpActivity.class);
        startActivity(signUpIntent);
    }

    public void goToHomeActivity(View view) {
        userName = ((EditText) findViewById(R.id.email)).getText().toString();
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.putExtra("userName", userName);
        startActivity(homeIntent);
    }
}

