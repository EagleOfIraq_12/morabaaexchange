package team.morabaa.com.morabaaexchange;

import android.content.Context;
import android.content.Intent;

import team.morabaa.com.morabaaexchange.utils.SaveSharedPreference;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * Created by eagle on 8/15/2017.
 */

public class PublicClass {
    static String fontCiro="fonts/Cairo.ttf";

    static void signout(Context ctx){
        SaveSharedPreference.removeUserName(ctx);
        Intent intent=new Intent(ctx,LoginActivity.class);
        startActivity(ctx,intent,null);

    }
}
