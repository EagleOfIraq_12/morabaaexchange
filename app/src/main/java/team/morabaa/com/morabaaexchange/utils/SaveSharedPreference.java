package team.morabaa.com.morabaaexchange.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {
    private static String PREF_USER_NAME = "morabaaexchangeusername";
    private static String PREF_PASSWORD = "morabaaexchangepassword";

    private static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName) {
        SharedPreferences editor = getSharedPreferences(ctx);
        editor
                .edit()
                .putString(PREF_USER_NAME, userName)
                .apply();
    }
    public static void removeUserName(Context ctx) {
        SharedPreferences editor = getSharedPreferences(ctx);
        editor
                .edit()
                .remove(PREF_USER_NAME)
                .apply();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setPassword(Context ctx, String password) {
        SharedPreferences editor = getSharedPreferences(ctx);
        editor
                .edit()
                .putString(PREF_PASSWORD, password)
                .apply();
    }

    public static String getPassword(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_PASSWORD, "");
    }
}
